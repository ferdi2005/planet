<?php
function lugheader ($title, $tagline) { ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="italian" />
	<meta name="robots" content="noarchive" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="https://www.linux.it/shared/index.php?f=bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="https://www.linux.it/shared/index.php?f=main.css" rel="stylesheet" type="text/css" />

	<meta name="dcterms.creator" content="Italian Linux Society" />
	<meta name="dcterms.type" content="Text" />
	<link rel="publisher" href="http://www.ils.org/" />

	<meta name="twitter:title" content="<?php echo $tagline ?>" />
	<meta name="twitter:creator" content="@ItaLinuxSociety" />
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:url" content="http://planet.linux.it/" />
	<meta name="twitter:image" content="http://planet.linux.it/images/tw.png" />

	<meta property="og:site_name" content="<?php echo $title ?>" />
	<meta property="og:title" content="<?php echo $title ?>" />
	<meta property="og:url" content="http://planet.linux.it/" />
	<meta property="og:image" content="http://planet.linux.it/images/fb.png" />
	<meta property="og:type" content="website" />
	<meta property="og:country-name" content="Italy" />
	<meta property="og:email" content="webmaster@linux.it" />
	<meta property="og:locale" content="it_IT" />
	<meta property="og:description" content="<?php echo $tagline ?>" />

	<script type="text/javascript" src="https://www.linux.it/shared/index.php?f=jquery.js"></script>
	<script type="text/javascript" src="https://www.linux.it/shared/index.php?f=bootstrap.js"></script>

	<?php header_eventi(); ?>

	<title><?php echo $title ?></title>

	<link rel="alternate" type="application/rss+xml" title="Planet LUG" href="https://planet.linux.it/lug/rss.xml" />
	<link rel="alternate" type="application/rss+xml" title="Planet ILS" href="https://planet.linux.it/ils/rss.xml" />
</head>

<body>

<div id="header">
	<img src="/images/logo.png" alt="<?php echo $title ?>" />
	<div id="maintitle"><?php echo $title ?></div>
	<div id="payoff"><?php echo $tagline ?></div>

	<div class="menu">
		<a class="generalink" href="/lug/">Planet LUG</a>
		<a class="generalink" href="/ils/">Planet ILS</a>
		<a class="generalink" href="/eventi/">Eventi</a>
		<a class="generalink" href="/contatti/">Contatti</a>

		<p class="social mt-2">
			<a rel="nofollow" href="https://twitter.com/ItaLinuxSociety"><img src="https://www.linux.it/shared/?f=immagini/twitter.svg" alt="ILS su Twitter"></a>
			<a rel="nofollow" href="https://www.facebook.com/ItaLinuxSociety/"><img src="https://www.linux.it/shared/?f=immagini/facebook.svg" alt="ILS su Facebook"></a>
			<a rel="nofollow" href="https://www.instagram.com/ItaLinuxSociety"><img src="https://www.linux.it/shared/?f=immagini/instagram.svg" alt="ILS su Instagram"></a>
			<a rel="nofollow" href="https://mastodon.uno/@ItaLinuxSociety/"><img src="https://www.linux.it/shared/?f=immagini/mastodon.svg" alt="ILS su Mastodon"></a>
			<a rel="nofollow" href="https://gitlab.com/ItalianLinuxSociety/Planet"><img src="https://www.linux.it/shared/index.php?f=immagini/gitlab.svg" alt="Planet su GitLab"></a>
		</p>
	</div>
</div>

<br>

<?php
}

function lugfooter () {
?>

<div id="ils_footer" class="mt-5">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<span style="text-align: center; display: block">
					<a href="http://www.gnu.org/licenses/agpl-3.0-standalone.html" rel="license">
						<img src="https://www.linux.it/shared/index.php?f=immagini/agpl3.svg" style="border-width:0" alt="AGPLv3 License">
					</a>

					<a href="http://creativecommons.org/publicdomain/zero/1.0/deed.en_US" rel="license">
						<img src="https://www.linux.it/shared/index.php?f=immagini/cczero.png" style="border-width:0" alt="Creative Commons License">
					</a>
				</span>
			</div>

			<div class="col-md-3">
				<h2>RESTA AGGIORNATO!</h2>
				<iframe title="Newsletter ILS" src="https://crm.linux.it/form/1" width="100%" height="420" frameBorder="0"><p>Your browser does not support iframes.</p></iframe>
			</div>

			<div class="col-md-3">
				<h2>Amici</h2>
				<p style="text-align: center">
					<a href="https://www.ils.org/info#aderenti">
						<img src="https://www.ils.org/external/getrandlogo.php" border="0" loading="lazy" alt="Aderenti a Italian Linux Society" /><br />
						Scopri tutte le associazioni che hanno aderito a ILS.
					</a>
				</p>
			</div>

			<div class="col-md-3">
				<h2>Network</h2>
				<script type="text/javascript" src="https://www.ils.org/external/widgetils.js" defer></script>
				<div id="widgetils"></div>
			</div>
		</div>
	</div>

	<div style="clear: both"></div>
</div>
</body>
</html>

<?php
}

function header_eventi() {
    ?>
    <link href="https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ical.js/1.4.0/ical.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/locales-all.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@fullcalendar/icalendar@5.8.0/main.global.min.js"></script>
    <script src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>
    <script src="https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js"></script>

    <link href="/assets/calendar.css" rel="stylesheet" type="text/css" />

    <script>
        document.addEventListener("DOMContentLoaded", function () {
            let calendarEl = document.getElementById("calendar");

            // If the calendar element is not present on the page do nothing
            if (calendarEl === null) {
              return;
            }

            let calendar = new FullCalendar.Calendar(calendarEl, {
                initialView: "dayGridMonth",
                locale: 'it',
                validRange: {
                    start: '<?php echo date('Y-m'); ?>-01',
                    end: '<?php echo date('Y-m-d', strtotime('+2 months')); ?>',
                },
                headerToolbar: {
                    left: "prev,next today",
                    center: "title",
                    right: "dayGridMonth,timeGridWeek,timeGridDay",
                },
                events: {
                <?php
                $file = "./eventi.ics";
                if ( isset($_GET['regione']) ) {
                    $check = preg_replace('/[^\p{L}\p{N}\s]/u', '', $_GET['regione']);
                    if (file_exists('./calendari/' . $check . '.ics')) {
                        $file = 'calendari/' . $check . '.ics';
                    }
                }
                ?>
                    url: '<?php echo $file ?>',
                    format: "ics",
                },
                eventDidMount: function(info) {
                    desc = info.event.extendedProps.description;
                    if (desc === null) {
                        desc = info.event._def['title'];
                    }
                    let doc = new DOMParser().parseFromString(desc, 'text/html');
                    desc = doc.body.textContent || "";
                    desc = desc.substring(0,400) + '...'
                    var tooltip = new Tooltip(info.el, {
                        title: desc,
                        placement: 'top',
                        trigger: 'hover',
                        container: 'body'
                    });
                },
            });

            calendar.render();
        });
    </script>
    <?php
}
