<?php
declare(strict_types=1);

namespace Core\Tests\Unit;

use PHPUnit\Framework\TestCase;

require_once('../../src/rss.php');

class rssTest extends TestCase
{
    /**
     * @test
     */
    public function it_removes_html_tags_from_messages_except_the_whitelisted_ones()
    {
        // Expectation: unwanted tags are stripped out
        $this->assertEquals('message', sanitizeString('<div>message</div>'));
        $this->assertEquals('message', sanitizeString('<script>message</script>'));
        $this->assertEquals('message', sanitizeString('<iframe>message</iframe>'));
        $this->assertEquals('message', sanitizeString('<table>message</table>'));

        // Expectation: allowed tags are not removed
        $messageWithAllowedTags = <<<HTML
<p>message</p><br>
<strong>strong</strong>
<b>bold</b>
<acronym>WHO</acronym>
<abbr title="World Health Organization">WHO</abbr>
<a href="https://linux.it">linux.it</a>
<del>message</del>
<strike>message</strike>
<code>code</code>
<em>message</em>
<i>message</i>
HTML;

        $this->assertEquals($messageWithAllowedTags, sanitizeString($messageWithAllowedTags));
    }
}
